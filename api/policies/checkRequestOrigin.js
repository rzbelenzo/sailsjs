import useragent from "useragent";
import uniqid from "uniqid";

function checkRequestOrigin (req, res, next) {
	const uuid = uniqid();

	const requestUseragent = useragent.parse(req.headers["user-agent"]);
	const requestId = req.headers?.["x-origin-request-id"] ?? uuid;
	const ip = req.headers?.["x-origin-user-ip"] ??
    (req.headers?.["x-forwarded-for"]?.split(",")[0].trim() || req.ip ||
    req._remoteAddress || (req.connection && req.connection.remoteAddress));
	const browser = req.headers?.["x-origin-user-browser"] ?? requestUseragent.toAgent();
	const device = req.headers?.["x-origin-user-device"] ?? requestUseragent.device.toString();
	const os = req.headers?.["x-origin-user-os"] ?? requestUseragent.os.toString();

	req.origin = {
		requestId,
		ip,
		browser,
		device,
		os,
	};

	return next();
}

export default checkRequestOrigin;
