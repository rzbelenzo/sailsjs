import flaverr from "flaverr";

const VerifyAccount = async (req, res, next) => {
	try {
		if (!req.headers["x-parse-auth"]) {
			throw Error("Missing Auth");
		}

		const auth = req.headers["x-parse-auth"];

		const checkSession = await User.find({
			authoritzation : auth,
		});

		if (_.isEmpty(checkSession)) {
			throw flaverr({
				statusCode: 400,
				message: checkSession.error,
			});
		}

		req.user = checkSession;

		return next();
	} catch (error) {
		sails.log.error("VerifyAccount:policies::error", error);

		return res.badRequest(error);
	}
};

export default VerifyAccount;
