import { pickBy } from "lodash";
import dayjs from "dayjs";
import onFinished from "on-finished";
import projectPackage from "../../package.json";

function logRequest(req, res, next) {
	// whole origin object can be only retrieved before the
	// response end
	const {
		origin: {
			requestId,
			ip,
			browser,
			device,
			os,
		},
		url,
		method,
	} = req;

	const startTime = dayjs().toDate();
	const serviceId = req.headers["service-id"];

	// do not include log service to be recorded
	if (serviceId === "log-service") {
		return next();
	}

	// attached on finished event listener on response
	// this function will run after the response sent to client
	onFinished(res, async (onFinishedError, res) => {
		const endTime = dayjs().toDate();
		const responseTime = dayjs(endTime).diff(startTime, "ms");

		// define log data
		let logData = {
			requestId,
			serviceName: projectPackage.name,
			httpCode: `${res.statusCode}`,
			errorMessage: res.error?.message ?? null,
			errorStack: res.error && res.error.hasOwnProperty("stack") ?
				JSON.stringify(res.error.stack) : null,
			clientIp: ip,
			clientUserAgent: {
				browser,
				device,
				os,
			},
			startTime: dayjs(startTime).format("YYYY-MM-DD HH:mm:ss SSS"),
			endTime: dayjs(endTime).format("YYYY-MM-DD HH:mm:ss SSS"),
			responseTime,
			serviceURI: url,
			method,
			user: req.user || {},
		};

		if (res.statusCode !== 404 && res.statusCode !== 401) {
			// route stack can be only used when response is about to end
			// so we can retrieve all the handler layers (middlewares and controller action)
			// on the req object
			const {
				route: { stack },
				options: { action },
			} = req;

			// last object on the layer will be the controller action
			const lastHandlerLayer = stack[stack.length - 1];
			// eslint-disable-next-line no-negated-condition
			const serviceAction = !_.isEmpty(action) ?
				action.split("/").slice(-1)[0] : lastHandlerLayer.name;

			logData = {
				...logData,
				serviceAction,
			};
		}

		// remove undefined and null values
		logData = pickBy(logData);

		await Logs.create(logData);
	});

	return next();
}

export default logRequest;
