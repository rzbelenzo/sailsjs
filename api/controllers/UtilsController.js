import flaverr from "flaverr";
import uniqid from "uniqid";

module.exports = {
	furtherverifyUserId: async (req, res) => {
		try {
			let { where = "{}"} = req.query;

			where = JSON.parse(where);
			const { id } = where;

			if (!id) {
				throw flaverr({
					statusCode: 400,
					message: "id is required!",
				});
			}

			const checkUser = await User.find(id);

			let data = checkUser;

			if (_.isEmpty(checkUser)){
				data = "user not found";
			}

			res.ok({
				data,
			});
		} catch (e) {
			sails.log.error("UtilsController:furtherverifyUserId:error");
			sails.log.error(e);

			res.badRequest(e);
		}
	},

	getIPAddress: async (req, res) => {
		try {
			const uuid = uniqid();

			const requestUseragent = useragent.parse(req.headers["user-agent"]);
			const requestId = req.headers?.["x-origin-request-id"] ?? uuid;
			const ip = req.headers?.["x-origin-user-ip"] ??
            (req.headers?.["x-forwarded-for"]?.split(",")[0].trim() || req.ip ||
            req._remoteAddress || (req.connection && req.connection.remoteAddress));
			const browser = req.headers?.["x-origin-user-browser"] ?? requestUseragent.toAgent();
			const device = req.headers?.["x-origin-user-device"] ?? requestUseragent.device.toString();
			const os = req.headers?.["x-origin-user-os"] ?? requestUseragent.os.toString();

			res.ok({
				data: {
					requestId,
					ip,
					browser,
					device,
					os,
				},
			});
		} catch (e) {
			sails.log.error("UtilsController:furtherverifyUserId:error");
			sails.log.error(e);

			res.badRequest(e);
		}
	},
};
