
module.exports = {
	find: async (req, res) => {
		try {
			// eslint-disable-next-line newline-after-var
			const finduser = await User.find();
			res.ok({
				data: finduser,
			});
		} catch (error) {
			sails.log.error("UserController:find::error");
			sails.log.error(error);
			res.badRequest(error);
		}
	},


	create: async (req, res) => {
		try {
			const {	body: {	accountId, name, lastname, firstname} } = req;

			const finduser = await User.create({
				accountId,
				name,
				lastname,
				firstname,
			});

			res.ok({
				data: finduser,
			});
		} catch (error) {
			sails.log.error("UserController:create::error");
			sails.log.error(error);
			res.badRequest(error);
		}
	},

	// destroy DELETE
	// update PATCH
};
