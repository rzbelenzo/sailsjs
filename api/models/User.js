module.exports = {
	attributes: {
		accountId: { type: "string" },
		authoritzation: { type: "string" },
		name: { type: "string" },
		lastname: { type: "string" },
		firstname: { type: "string" },
	},
};
