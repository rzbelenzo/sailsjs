module.exports = {
	tableName: "logs",
	attributes: {
		requestId: {
			type: "string",
		},
		serviceName: {
			type: "string",
		},
		serviceURI: {
			type: "string",
		},
		serviceAction: {
			type: "string",
		},
		method:{
			type: "string",
		},
		httpCode: {
			type: "string",
		},
		errorMessage: {
			type: "string",
			columnType: "longtext",
		},
		errorStack: {
			type: "string",
			columnType: "longtext",
		},
		clientIp: {
			type: "string",
		},
		clientUserAgent: {
			type: "json",
		},
		metadata: {
			type: "json",
		},
		startTime: {
			type: "string",
		},
		endTime: {
			type: "string",
		},
		responseTime: {
			type: "string",
		},
		user: {
			type: "json",
		},
	},
};
